package cn.com.smart.report.service;

import org.springframework.stereotype.Service;

import cn.com.smart.report.bean.entity.TReportField;
import cn.com.smart.service.impl.MgrServiceImpl;

@Service
public class ReportFieldService extends MgrServiceImpl<TReportField> {

}
